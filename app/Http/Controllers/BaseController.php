<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class BaseController extends Controller
{
    /**
	* 同步POST提交
	*
	* @param $url 提交url
	* @param $postDatas array 提交数据
	* @return $response string 返回数据
	*
	*/
	public static function postSync($url,array $postDatas)
	{	
		$ssl = substr($url, 0, 8) == "https://" ? true : false;
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 28);
	    if ($ssl) {
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 信任任何证书
	        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // 检查证书中是否设置域名
	    }
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);// 显示输出结果
	    curl_setopt($ch, CURLOPT_POST, true); // post传输
	    curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8888');
	    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postDatas));
	    curl_error($ch);
	    $response = curl_exec($ch);
	    curl_close($ch);
	    return $response;
	}

	/**
    * 成功返回json提示
    * @param code int 成功码
    * @param msg string 成功信息
    */
    public static function success($code = 1,$data='',$msg = "success")
    {
    	header('Content-Type:application/json; charset=utf-8');
    	exit(json_encode(['code'=>$code,'data'=>$data,'msg'=>$msg]));
    }

    /**
    * 失败返回json提示
    * @param code int 失败错误码
    * @param msg string 失败信息
    */
    public static function error($code = 0,$msg = "error")
    {
    	header('Content-Type:application/json; charset=utf-8');
    	exit(json_encode(['code'=>$code,'msg'=>$msg]));
    }
}
