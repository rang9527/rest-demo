<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Order;
use App\Http\Controllers\BaseController as Base;

class OrdersController extends Controller
{
	/**
	* 订单列表
	*/
    public function index()
    {
    	$orders = Order::all();
    	Base::success(1001,$orders->toArray());
    }

    /**
	* 订单创建
	*/
    public function create(Request $request)
    {
    	// 检查参数合法性
    	$this->validate($request, [
            'goods' => 'required|max:2',
            'price' => 'numeric'
        ]);

    	parse_str($request->getContent().$request);

    	$order = new Order;
    	$order->goods = $request['goods'];
		$order->price = $request['price'];
		$order->customer_id = $request['customer_id'];
		$order->created_at = date('Y-m-d H:i:s');
		$order->save();

		Base::success(1001);

    }


    /**
    * 订单详情
    */
    public function orderDetail(Request $request,$id)
    {
    	$order = Order::findOrFail($id);

    	Base::success(1001,$order->toArray());
    }

    /**
	* 订单更新
	*/
	public function orderUpdate(Request $request,$id)
	{

    	parse_str($request->getContent(),$request) ;
    	$order = Order::findOrFail($id);
    	$order->goods = $request['goods'];
		$order->price = $request['price'];
		$order->customer_id = $request['customer_id'];
		$order->save();

		Base::success(1001);
	}

	/**
	* 订单删除
	*/
	public function orderDelete(Request $request,$id)
	{
		$order = Order::findOrFail($id);
		$order->delete();

		Base::success(1001);
	}




}
