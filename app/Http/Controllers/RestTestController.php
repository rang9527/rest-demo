<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Httpful\Request as HttpRequest;

class RestTestController extends Controller
{
    public function github()
	{
		$uri = 'https://api.github.com/users/rang9527';
		$request = HttpRequest::get($uri)->send();

		// var_dump($request);

		echo "{$request->body->name} joined GitHub on " . date('Y-m-d H:i:s', strtotime($request->body->created_at)) ."\n";
	}

	/***************************以下模拟客户端**************************************/


	/**
	* 创建订单
	*
	*/
	public function orderCreate()
	{

		// dd('ddddd');

		$params = ['goods'=>'测试商品','price'=>'11.2','customer_id'=>3];
		$paramsJson = json_encode($params);

		$uri = 'http://localhost:8000/orders';
		$response = HttpRequest::post($uri)
	    ->body($paramsJson)
	    ->sendsJson()
	    ->send();

	    // echo $response->body;
	    var_dump($response);
	}



}
