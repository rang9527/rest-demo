<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});




Route::group(['prefix'=>'/','middleware'=>'throttle:60'],function(){

	// orders
	Route::get('orders', 'OrdersController@index');
	Route::post('orders','OrdersController@create');

	Route::get('orders/{id}','OrdersController@orderDetail');
	Route::put('orders/{id}','OrdersController@orderUpdate');
	Route::delete('orders/{id}','OrdersController@orderDelete');

	// customers

	Route::get('customers','CustomersController@index');
	Route::post('customers','CustomersController@create');

	Route::get('customers/{id}','CustomersController@customerDetail');
	Route::put('customers/{id}','CustomersController@customerUpdate');
	Route::delete('customers/{id}','CustomersController@customerDelete');

	Route::get('customers/{id}/orders','CustomersController@customerOrders');
	Route::post('customers{id}/orders','CustomersController@customerOrdersCreate');
	Route::delete('customers{id}/orders','CustomersController@customerOrdersDelete');
});





